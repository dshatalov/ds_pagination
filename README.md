**Javascript Pagination**

Pagination for your javascript list.  Very simple installation. You set total items count and do your actions after page click.
CSS and JS are simple and pure.

No libraries required
You can find demo here: https://dshatalov.com/demos/ds_pagination

---

## Intro


    var pagination = new DSPagination({
        "items_per_page"    : 10,     // How many list items on one page
        "display_pages"     : 10,	  // How many pages to display 
        "total_items_count" : 100,	  // Required, your total list items 
        "console_log"       : false,  // Optional, default is false. 
        "target_id"         : "your_pagination_id"
    });

    /* Function is called right before setting new page as current */
    pagination.before_page_set = function(page)
    {
        console.log("before_page_set", page);
    }

	/* Current page already set and new pagination is rendered. Use this to do you actions after page select */
    pagination.on_page_select = function(page)
    {
        console.log("on_page_select", page);
    }

If you need change logic of page selection, like stop render new pagination and etc, you can change function below. It is inner function and by default is not designed for outside rewrite.  

    /* This is inner function. If you want to change logic of page selection you may change it*/
	set_page(page)
	{
		this.set("current_page", page);
		this.start();
	}
---
